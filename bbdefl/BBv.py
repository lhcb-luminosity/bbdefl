from BB import BB


def _mlen(x):
    try:
        return len(x)
    except:
        return 1


def _rep(x, n):
    return ([x]*n if isinstance(x, (int, float)) else x)


def BBv(Csigx, Csigy, sepx, sepy, betax, betay, tunex, tuney, Np, Ep):
    """Vectorized version of BB"""
    names = ['Csigx', 'Csigy', 'sepx', 'sepy', 'betax', 'betay', 'tunex', 'tuney', 'Np', 'Ep']
    args = locals()
    args = {k: args[k] for k in names}
    lens = map(_mlen, args.values())
    n = max(lens)
    if not all(x == n or x == 1 for x in lens):
        raise Exception('All arguments must be of same length or scalar')

    args = {k: _rep(v, n) for k, v in args.items()}

    allres = [[], [], [], []]
    for i in range(n):
        kwargs = {k: v[i] for k, v in args.items()}
        res = BB(**kwargs)
        for j in range(4):
            allres[j].append(res[j])
    return allres
