import numpy.distutils.core

errfff = numpy.distutils.core.Extension(
    name='bbdefl.errfff',
    sources=['bbdefl/errfff.f'],
)

numpy.distutils.core.setup(
    name='bbdefl',
    version='1.1',
    description='Tool to calculate beam-beam deflections',
    author='T. Pieloni and W. Kozanecki',
    packages=['bbdefl'],
    package_data={'': ['errfff.f', 'examples/*.py']},
    ext_modules=[errfff],
)
