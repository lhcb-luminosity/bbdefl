## Introduction
This is a packaged version of the code provided by T. Pieloni and W. Kozanecki
[here](http://witold.web.cern.ch/witold/TEMP/LHC_transit/bbDefl/bbDefl_v1.1.tar)
for calculating beam-beam deflections.
The only addition is a vectorized version of the `BB` function called `BBv`.

## Installation and use
```sh
pip install --upgrade --user git+https://gitlab.cern.ch/lhcb-luminosity/bbdefl.git
```

```python
import bbdefl
bbdefl.BB(Csigx, Csigy, sepx, sepy, betax, betay, tunex, tuney, Np, Ep)

# and for an interactive examples do
import bbdefl.examples.BBscanTest
```

## History
(copied from [BBtestScan.py](bbdefl/examples/BBscanTest.py))
### VERSION 1.0
#### T. Pieloni and W. Kozanecki, 26 April 2013

This Python script (BBscanTest.py) exercises the beam-beam deflection and the single-beam orbit shift
that occur during a centered or off-axis beam-beam scan at an LHC IP. The deflection and orbit shift
are calculated in the Python function BB.py, using the Basseti-Erskine formula applied to gaussian,
untilted elliptical beams.

Based on the input arguments detailed below, the function BB.py calculates the beam-beam deflection
angle and the single-beam orbit shift for one step in a beam-separation scan in either the x-plane or
the y-plane. This function is designed to be used in a stand-alone, plug-in function in any other
analysis program; it only requires, as an ancillary function, the pre-compiled Fortran subroutine
errffor.so (that replaces the numerically unstable Python version of the complex error function).

For debugging purposes, the present script (BBscanTest.py) also calculates the beam-beam kick
and the associated orbit shift in the round-beam limit, using the simplified analytical formula
applicable to that case.

Note that in this first version:
- the beam energy is hard-coded to 4 TeV per beam inside BB.py
- the formulas are only valid for pp collisions. p-Pb and Pb-Pb collisions are not yet supported.

Usage:
- specify the input bunch parameters in BBscanTest.py
- specify the input scan parameters in BBscanTest.py
- type (e.g. on lxplus): python BBscanTest.py. 2 plots will appear:
  the deflection angle & the orbit shift as a function of the beam separation


### VERSION 1.1
#### T. Pieloni  15 July 2013

New since Version 1.0:
- the beam energy (in TeV) is no longer hard-coded in BB.py, but is now an input argument to BB.py;
- the script can be used to calculate beam-beam deflections in the p-Pb, Pb-p & p-p configurations,
  simply by
  (a) using the equivalent proton energy (e.g. 3.5 TeV)
      instead of the mean nuclean energy (e.g. 1.38 TeV)
  (b) entering the bunch populations as bunch charges (i.e. number of protons), as usual.
